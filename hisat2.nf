#!/usr/bin/env nextflow

process hisat2_build {
// Runs hisat2-build
//
// input:
//   path fa - Reference FASTA
//   val params - Additional Parameters
//
// output:
//   tuple => emit: idx_files
//     path(fa) - Reference FASTA
//     path("${fa}.*") - Index Files

// require:
//   params.hisat2$dna_ref
//   params.hisat2$hisat2_index_parameters

  storeDir "${params.shared_dir}/${fa}/hisat2_build"
  tag "${fa}"
  label 'hisat2_container'
  label 'hisat2_index'

  input:
  path fa
  val parstr

  output:
  tuple path(fa), path("*.ht2"), emit: idx_files

  script:
  """
  FA_BUFFER=\$(echo ${fa})
  hisat2-build ${parstr} ${fa} \${FA_BUFFER%.fa}
  """
}


process hisat2 {
// Runs hisat2
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   IDX_FILES
//   params.hisat2$hisat2_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'hisat2_container'
  label 'hisat2'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.sam'), emit: sams

  script:
  """
  hisat2 \
  -q \
  -x ${fa} \
  -1 ${fq1} \
  -2 ${fq2} \
  -p ${task.cpus} \
  > ${dataset}-${pat_name}-${run}.sam
  """
}


process hisat2_samtools_sort {
// Runs hisat2 piped to samtools sort (to minimize storage footprint)
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*.bam") - Output BAM File

// require:
//   FQS
//   IDX_FILES
//   params.hisat2$hisat2_mem_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'hisat2_samtools_container'
  label 'hisat2_samtools'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*sorted.bam'), emit: bams

  script:
  """
  FA_BUFFER=\$(echo ${fa})
  hisat2 \
  -q \
  -x \${FA_BUFFER%.fa} \
  -1 ${fq1} \
  -2 ${fq2} \
  -p ${task.cpus} \
  > tmp.bam
  samtools sort -@ ${task.cpus} tmp.bam > ${dataset}-${pat_name}-${run}.sorted.bam
  rm tmp.bam
  """
}
